#include <iostream>
#include <algorithm>
#include <list>
#include <vector>
#include <set>

using namespace std;

int main()
{
	srand(time(NULL));
	vector<int> newVector;
	vector<int> sortVector = { 1,2,3,4,5 };
	vector<int>::iterator it;
	set<int> boundSet = { 1,2,3,4,5,6,7,8,9,10 };
	set<int>::iterator itlow, itup;
	set<int> s1 = { 1,2,3,4,5,6,7,8 };
	set<int> s2 = { 3,4,5,6 };
	set<int> diffVector;
	for (int i = 0; i < 10; i++)
	{
		newVector.push_back(0);
	}
	cout << "Generate:" << endl;
	for (auto i : newVector)
	{
		cout << i << " ";
	}
	cout << endl;

	generate(newVector.begin(), newVector.end(), []()->int
		{
			return rand() % 10;
		});
	for (auto i : newVector)
	{
		cout << i << " ";
	}
	cout << endl;
	cout << "Partial sort copy:" << endl;
	it = partial_sort_copy(newVector.begin(), newVector.end(), sortVector.begin(), sortVector.end());
	for (auto i : newVector)
	{
		cout << i << " ";
	}
	cout << endl;
	for (auto i : sortVector)
	{
		cout << i << " ";
	}
	cout << endl;
	cout << "Set lower/upper bound:" << endl;
	itlow = boundSet.lower_bound(4);
	itup = boundSet.upper_bound(8);
	boundSet.erase(itlow, itup);
	for (auto i : boundSet)
	{
		cout << i << " ";
	}
	cout << endl;
	cout << "Set difference:" << endl;
	set_difference(s1.begin(), s1.end(), s2.begin(), s2.end(),
		inserter(diffVector, diffVector.end()));
	for (auto i : diffVector)
	{
		cout << i << " ";
	}
}